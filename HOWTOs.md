# Elionic - HOWTOs

An open-source project to test both **Ionic** and **Electron**.

**Elionic** is an *Ionic* app launched into an *Electron* frame.

==================================

### How to test only the Ionic app in a local browser:

```bash
        cd elionic-demo/ionic
        ionic serve -lcs
```
    
### How to package for Windows:
To package the app for Windows run:

```bash
        cd elionic-demo
        npm run package-win
``` 

The file **Electron.exe** is created in the folder *elionic-demo\release-builds\Elionic-win32-ia32*

### How to produce an apk to test the app on an Android phone:

```bash
    cd elionic-demo/ionic
    ionic cordova build android --prod
```
The file **android-debug.apk** is created in the folder *elionic-demo\ionic\platforms\android\build\outputs\apk*

### How to create an installer for Windows:

```bash
    cd elionic-demo
    npm run release
```

The files **Elionic Desktop.exe** and **Elionic Desktop Setup.exe** are created in the folder *elionic-demo\dist*

