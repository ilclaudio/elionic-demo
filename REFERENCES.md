# Elionic - References

An open-source project to test both **Ionic** and **Electron**.

**Elionic** is an *Ionic* app launched into an *Electron* frame.

==================================

This project started thanks to this forum post: https://forum.ionicframework.com/t/is-ionic3-pwa-the-best-choice


To implement this demo I've used the suggestions of that post and these tutorials:

* [Creating Cross-platform Apps with Ionic and Electron](https://app.pluralsight.com/library/courses/ionic-electron-building-cross-platform-apps)
  by **Michael Callaghan** on Pluralsight.
  
* [Creating Desktop Apps with Electron Tutorial](https://coursetro.com/courses/22/Creating-Desktop-Apps-with-Electron-Tutorial) by **Gary Simon** (Coursetro.com) on Youtube.

* [Electron official documentation](https://electronjs.org/docs).

* [Ionic official documentation](https://ionicframework.com/docs).

* [ng2-charts - Angular directives for Chart.js](https://valor-software.com/ng2-charts/).

* [js-xlsx with Electron](https://github.com/SheetJS/js-xlsx/tree/master/demos/electron).

