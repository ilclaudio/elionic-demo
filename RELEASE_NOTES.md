# Elionic - Release notes

An open-source project to test both **Ionic** and **Electron**.

**Elionic** is an *Ionic* app launched into an *Electron* frame.

==================================

###Features already implemented:

* Electron: Added an Ionic App into a frame.
* Electron: Added a custom menu.
* Electron: Enabled the upload of native file.
* Ionic: Added a chart done with **ng2-charts**.
* Electron: Excel to json with **js-xlsx**.
* Electron: Added a splash screen.
* Electron: Added an 'About' page.
* Electron/Ionic main refactoring: moved the code of the two components in different subdirectories (***app*** and ***ionic***).
* Electron: Added the configuration to produce Windows executable.
* Ionic: Added the configuration to produce an Android apk.
* Electron: Added the configuration to produce a Windows installer.
* Electron: Published on GihHub the apk, the executable, the portable and the installer for Windows.
* Ionic: Icona app e splashscreen app

###TODO LIST:
* Electron: Refactoring of code, move functions from main.js to classes.
* Electron: call a Json Rest Web Service.
* Ionic: call a Json Rest Web Service.
* Ionic: push notifications.
* Electron: add a service provider.
* Ionic: add a service provider.
* Electron: Drag&Drop of native files.
* Electron: inter processes comunication.
* Electron: Add configuration for autoaupdating the app.
* Ionic: Add authentication.
* Electron: Add authentication.
* Electron: Publish .dmg for Apple.
