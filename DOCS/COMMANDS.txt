###############  DISTRIBUZIONE DEL SOFTWARE ##################

- Creazione APK (da elionic-demo/ionic) :
    ionic cordova build android --prod --release
    ionic cordova build android --prod

-Generazione Chiave:
    keytool -genkey -v -keystore elionic.keystore -alias elionic -keyalg RSA -keysize 2048 -validity 10000

- Pubblicazione APP:
    ionic cordova build --release android
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore elionic.keystore Elionic-03-unsigned.apk elionic
    zipalign -v 4 Elionic-02-unsigned.apk Elionic.apk

-  Realizzazione eseguibile windows (da elionic-demo):
    npm run package-win

- Realizzazione installer windows da elionic-demo):
    npm run create-installer-win

###############  FINE DISTRIBUZIONE DEL SOFTWARE ##################



### Come aggiornare moduli ###

npm install electron -g
npm update -g cordova
npm update -g ionic

### Come aggiornare NPM (di NodeJs) ###
Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force
npm install -g npm-windows-upgrade
npm-windows-upgrade
