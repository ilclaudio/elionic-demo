import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class WsmanagerProvider {

  apiUrlCrypto = 'https://www.cryptocompare.com/api/data';
  apiUrlUsers = 'https://jsonplaceholder.typicode.com';
  cryptoHeaders = null;
  userHeaders = null;

  constructor(public http: HttpClient) {

    this.cryptoHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods':'POST, GET, OPTIONS, PUT',
    });

    this.userHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods':'POST, GET, OPTIONS, PUT',
    });
  }

  getCoinList() {
    return this.http.get(this.apiUrlCrypto+'/coinlist',{headers: this.cryptoHeaders}).subscribe(data => {
          console.log("Coins:",data);
        }, err => {
          console.log(err);
        });

    // return new Promise(resolve => {
    //   this.http.get(this.apiUrlCrypto+'/coinlist',{headers: this.cryptoHeaders}).subscribe(data => {
    //     resolve(data);
    //   }, err => {
    //     console.log(err);
    //   });
    // });

  }

  getUserList() {
    return this.http.get(this.apiUrlUsers+'/users').subscribe(data => {
      console.log("Users:",data);
    }, err => {
      console.log(err);
    });


    // return new Promise(resolve => {
    //   this.http.get(this.apiUrlUsers+'/users').subscribe(data => {
    //     resolve(data);
    //   }, err => {
    //     console.log(err);
    //   });
    // });
  }

}
