import { Component } from '@angular/core';

import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { ChartsPage } from '../charts/charts';
import { WebservicesPage } from '../webservices/webservices';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ChartsPage;
  tab3Root = WebservicesPage;
  tab4Root = ContactPage;

  constructor() {

  }
}
