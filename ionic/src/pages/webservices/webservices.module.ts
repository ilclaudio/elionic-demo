import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WebservicesPage } from './webservices';

@NgModule({
  declarations: [
    WebservicesPage,
  ],
  imports: [
    IonicPageModule.forChild(WebservicesPage),
  ],
})
export class WebservicesPageModule {}
