import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WsmanagerProvider } from '../../providers/wsmanager/wsmanager';


@IonicPage()
@Component({
  selector: 'page-webservices',
  templateUrl: 'webservices.html',
})
export class WebservicesPage {

  users: any;
  coins: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public wsManager: WsmanagerProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WebservicesPage');
  }

  getCoins(){
    console.log('getCoinsList');
    this.users = [];
    this.coins = [];
    this.coins = this.wsManager.getCoinList();

    // this.wsManager.getCoinList()
    //   .then(data => {
    //     this.coins = data;
    //     console.log("lenght:", this.coins.lenght);
    //   });
  }

  getUsers(){
    console.log('getUsers');
    this.coins = [];
    this.users = [];
    this.users = this.wsManager.getUserList();

    // this.wsManager.getUserList()
    //   .then(data => {
    //     this.users = data;
    //   });
  }

}
