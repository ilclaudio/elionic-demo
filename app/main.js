const {app, BrowserWindow, Menu} = require('electron');
const { autoUpdater } = require('electron-updater');

const path = require('path');
const fs = require('fs');
const url = require('url');
const isDev = require('electron-is-dev');

// Setup logger
autoUpdater.logger = require('electron-log');
autoUpdater.logger.transports.file.level = 'info';

// Setup updater events
autoUpdater.on('checking-for-update', () => {
  console.log('Checking for updates...');
});

autoUpdater.on('update-available', (info) => {
  console.log('Update available');
  console.log('Version', info.version);
  console.log('Release date', info.releaseDate);
});

autoUpdater.on('update-not-available', () => {
  console.log('Update not available');
});

autoUpdater.on('download-progress', (progress) => {
  console.log(`Progress ${Math.floor(progress.percent)}`);
});

autoUpdater.on('update-downloaded', (info) => {
  console.log('Update downloaded');
  autoUpdater.quitAndInstall();
});

autoUpdater.on('error', (error) => {
  console.error(error);
});

const SPLASHSCREEN_MILLISECONDS = 3000;

const openAboutWindow = require('about-window').default;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;
let splashScreen;
const iconPath = path.join(__dirname, 'assets','icons');
const windowIcon = 'logo.png';

function callOpenAboutWindow(){
  openAboutWindow({
    icon_path:  path.join(__dirname, 'assets', 'icons', 'ship_200.png'),
    copyright: 'Copyright (c) 2018 CBC',
    open_devtools: false
  });
}

function createWindow () {
  // Creazione della finestra del browser.
  win = new BrowserWindow({
    width: 900,
    height: 600,
    title: 'Elionic',
    icon: path.join(iconPath, windowIcon),
    show: false
    });

  // Load the Ionic App
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'www', 'index.html'),
    protocol: 'file:',
    slashes: true
  }));


  pathname: path.join(__dirname, 'www', 'index.html'),

  // Open the DevTools.
  //win.webContents.openDevTools();

  win.once('ready-to-show', () => {
    if (splashScreen && splashScreen.isVisible()) {
      splashScreen.destroy();
      splashScreen = null;
    }
    if (!win.isVisible()) {
      win.show();
    }
    // if (page) {
    //   navigateTo(page)
    // }
  });

  win.on('closed', () => {
    // Eliminiamo il riferimento dell'oggetto window;  solitamente si tiene traccia delle finestre
    // in array se l'applicazione supporta più finestre, questo è il momento in cui
    // si dovrebbe eliminare l'elemento corrispondente.
    win = null;
  });

}

const {shell} = require('electron');
const os = require('os');
const {dialog} = require('electron');

let menu = Menu.buildFromTemplate([
  {
    label: 'Menu',
    submenu: [
      {
        label: 'Choose the files...',
        click(){
          //shell.showItemInFolder(os.homedir());
          let l_aPathsChosen = dialog.showOpenDialog({
          properties: ['openFile',],
          filters: [
            {name: 'Excel old type', extensions: ['xls']},
            {name: 'Excel new type', extensions: ['xlsx']},
            ]
          });
          console.log(l_aPathsChosen);
          if (l_aPathsChosen.length>=1){
            // Considering only the first file with multi selection
            l_sPathFileExcel = l_aPathsChosen[0];
            let XLSX = require('xlsx');
            let workbook = XLSX.readFile(l_sPathFileExcel);
            // considering only the first sheet of the workbook
            let sheet_name_list = workbook.SheetNames;
            console.log(sheet_name_list[0]);
            let l_sJson = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
            console.log(l_sJson);
            dialog.showMessageBox({ message: JSON.stringify(l_sJson), buttons: ["OK"] });
          }

        }
      },
      {
        label: 'About',
        click(){
          callOpenAboutWindow();
        }
      },
      {
        label: 'Exit ',
        click(){
          app.quit()
        }
      }
    ]
  },
  {
    label: 'Documentation',
    submenu: [
      {
        label: 'Electron docs',
        click(){
          shell.openExternal('https://electronjs.org/docs');
        }
      },
      {
        label: 'Ionic docs',
        click(){
          shell.openExternal('https://ionicframework.com/docs');
        }
      },
    ]
  },
]);

Menu.setApplicationMenu(menu);

// Questo metodo viene chiamato quando Electron ha finito
// l'inizializzazione ed è pronto a creare le finestre browser.
// Alcune API possono essere utilizzate solo dopo che si verifica questo evento.
app.on('ready', appReady);

function appReady() {

  // Trigger update check
  if (!isDev) {
    autoUpdater.checkForUpdates();
  }

  Menu.setApplicationMenu(menu);

  // Only MacOS will have a dock property.
  if (app.dock) {
    app.dock.setIcon(path.join(iconPath, windowIcon))
  }

  // Show the SplashScreen
  createSplashScreen();

  // Keep showing the SplashScreen
  sleep(SPLASHSCREEN_MILLISECONDS).then(() => {
    createWindow();
  });

}

// Terminiamo l'App quando tutte le finestre vengono chiuse.
app.on('window-all-closed', () => {
  // Su macOS è comune che l'applicazione e la barra menù
  // restano attive finché l'utente non esce espressamente tramite i tasti Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', () => {
  // Su macOS è comune ri-creare la finestra dell'app quando
  // viene cliccata l'icona sul dock e non ci sono altre finestre aperte.
  if (win === null) {
    createWindow();
  }
});

function createSplashScreen() {
  splashScreen = new BrowserWindow({
    width: 900,
    height: 600,
    titleBarStyle: 'hidden',
    alwaysOnTop: true,
    closable: false,
    skipTaskbar: true,
    show: true,
    minimizable: false,
    maximizable: false,
    resizable: false,
    center: true,
    frame: false
  });

  splashScreen.loadURL(url.format({
    pathname: path.join(__dirname, 'assets', 'images', 'elionicsplashscreen.png'),
    protocol: 'file:',
    slashes: true
  }));

}

// sleep time expects milliseconds
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
