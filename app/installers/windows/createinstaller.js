const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller;
const path = require('path');

getInstallerConfig()
  .then(createWindowsInstaller)
  .catch((error) => {
    console.error(error.message || error);
    process.exit(1);
  });

function getInstallerConfig () {
  console.log('creating windows installer');
  const rootPath = path.join('./');
  const outPath = path.join(rootPath, 'release-builds');
  console.log(path.join(rootPath, 'app', 'assets', 'icons', 'ship_256.ico'));
  return Promise.resolve({
    appDirectory: path.join(outPath, 'Elionic-win32-ia32/'),
    authors: 'cb',
    noMsi: true,
    outputDirectory: path.join(outPath, 'windows-installer'),
    exe: 'Elionic.exe',
    setupExe: 'ElionicInstaller.exe',
    setupIcon: path.join(rootPath, 'app', 'assets', 'icons', 'ship_256.ico')
  });
}
